terraform {
  backend "s3" {
    bucket         = "recipe-app-api-tform-tfstate"
    key            = "recipe-app.tfstate"
    region         = "eu-west-3"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-3"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
data "aws_caller_identity" "current" {}